using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMeetingOfTheDay : MonoBehaviour
{
    public GameObject prefab;
    public Transform parent;
    void Start()
    {
        for (var i = 0; i < 10; i++)
        {
            Instantiate(prefab, parent.position, parent.rotation,parent);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
