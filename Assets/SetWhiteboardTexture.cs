using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWhiteboardTexture : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Whiteboard _whiteboard;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = _whiteboard.texture;
    }
}
