using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAndDeactivateControllersOrHands : MonoBehaviour
{
    [SerializeField] private List<MonoBehaviour> activeWithHands;

    [SerializeField] private List<MonoBehaviour> activeWithControllers;
    // Start is called before the first frame update
    

    // Update is called once per frame
    void Update()
    {
        bool isHandTrackingEnabled = OVRPlugin.GetHandTrackingEnabled();
        foreach (MonoBehaviour monoBehaviour in activeWithHands)
        {
            monoBehaviour.enabled = isHandTrackingEnabled;
        }

        foreach (MonoBehaviour monoBehaviour in activeWithControllers)
        {
            monoBehaviour.enabled = !isHandTrackingEnabled;
        }
    }
}
