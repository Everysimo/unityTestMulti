using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class DeactivateOnNotMine : MonoBehaviour
{
    [SerializeField] private PhotonView _photonView;
    [SerializeField] private List<MonoBehaviour> deactivateOnNotMine;

    // Start is called before the first frame update
    void Start()
    {
        if (!_photonView.IsMine)
        {
            foreach (MonoBehaviour monoBehaviour in deactivateOnNotMine)
            {
                monoBehaviour.enabled = false;
            }
        }
    }

    
}
