using System.Collections;
using System.Collections.Generic;
using Oculus.Avatar2;
using Photon.Pun;
using UnityEngine;

public class AvatarHandsNetworked : OvrAvatarCustomHandPose
{
    // Start is called before the first frame update
    public bool isHands;
    public bool isLeft;

    void Awake()
    {
        if (isHands)
        {
            if (isLeft)
            {
                _handPose = GameObject.FindWithTag("LeftInteractionHand");
                _wristOffset = GameObject.FindWithTag("LeftWristOffset").transform;
            }
            else
            {
                _handPose = GameObject.FindWithTag("RightInteractionHand");
                _wristOffset = GameObject.FindWithTag("RightWristOffset").transform;
            }
        }
        else
        {
            if (isLeft)
            {
                _handPose = GameObject.FindWithTag("LeftInteractionHandController");
                _wristOffset = GameObject.FindWithTag("LeftControllerWristOffset").transform;
            }
            else
            {
                _handPose = GameObject.FindWithTag("RightInteractionHandController");
                _wristOffset = GameObject.FindWithTag("RightControllerWristOffset").transform;
            }
        }
    }
}