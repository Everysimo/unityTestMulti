using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine;

public class NetworkMarker : WhiteboardMarker
{
    public void Awake()
    {
        PhotonPeer.RegisterType(typeof(DrawRequest), 255, DrawRequest.ObjectToByteArray,DrawRequest.ByteArrayToObject);

    }
    // Start is called before the first frame update

    [SerializeField] private PhotonView _photonView;
    
    protected override void SendRequest(DrawRequest drawRequest)
    {
        if (_photonView.IsMine)
        {
            int whiteboardId = _whiteboard.transform.GetComponent<PhotonView>().ViewID;
            _photonView.RPC("SendDrawRequestOverNetwork",RpcTarget.All, whiteboardId,drawRequest.From,drawRequest.To);
        }
    }

    [PunRPC]
    public void SendDrawRequestOverNetwork(int punId,Vector2 From,Vector2 To)
    {
        Whiteboard whiteboard = PhotonNetwork.GetPhotonView(punId).transform.GetComponent<Whiteboard>();
        DrawRequest drawRequest = new DrawRequest(_penSize, _colors, From, To);
        whiteboard.AddDrawRequest(drawRequest);
    }
    
}
