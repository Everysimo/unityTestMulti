using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyLocalRotationWithOffset : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Transform _transformToCopy;

    [SerializeField] private Vector3 offsetRotation;
    [SerializeField] private Vector3 offsetPosition;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion q = Quaternion.Euler(_transformToCopy.localRotation.eulerAngles + offsetRotation);
        transform.localRotation = q;
        transform.localPosition =  _transformToCopy.localPosition + offsetPosition;
    }
}
