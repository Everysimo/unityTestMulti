using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class Whiteboard : MonoBehaviour
{

    private Queue<DrawRequest> _requests;

    [HideInInspector]
    public Texture2D texture;
    public Vector2 textureSize = new Vector2(2048,2048);

    public bool copyTexture = false;
    [SerializeField]
    private Whiteboard _whiteboard;

    private Renderer r;

    private void Awake()
    {
        r = GetComponent<Renderer>();
        
        if(!copyTexture)
        {
            texture = new Texture2D((int)textureSize.x, (int)textureSize.y,TextureFormat.ARGB32,true);
            r.material.mainTexture = texture;
        }
       
    }

    void Start()
    {
        if (copyTexture)
        {
            texture = _whiteboard.texture;
            r.material.mainTexture = texture;
        }
        _requests = new Queue<DrawRequest>();

        Color[] colors = new Color[(int)textureSize.x];
        int mipCount = Mathf.Min((int)textureSize.x, texture.mipmapCount);

        // tint each mip level
        for (int mip = 0; mip < mipCount; ++mip)
        {
            Color[] cols = texture.GetPixels(mip);
            for (int i = 0; i < cols.Length; ++i)
            {
                cols[i] = r.material.color;
            }
            texture.SetPixels(cols, mip);
        }
        // actually apply all SetPixels, don't recalculate mip levels
        texture.Apply(false);
    }

    private void Update()
    {
        while (_requests.Count != 0)
        {
            DrawRequest request = _requests.Dequeue();
            Draw(request);
        }
        texture.Apply();
    }


    public void AddDrawRequest(DrawRequest drawRequest)
    {
        _requests.Enqueue(drawRequest);
    }


    private void Draw(DrawRequest request)
    {
        float distance = Vector2.Distance(request.From,request.To);

        int x = (int)request.To.x;
        int y = (int)request.To.y;

        texture.SetPixels(x, y, request.Size, request.Size,request.Colors);

        var interation = (request.Size / distance)/2;

        var i = 0;
                    
        for (float f = 0.01f; f < 1; f += interation)
        {
            var lerpX = (int)Mathf.Lerp(request.From.x, x, f);
            var lerpY = (int)Mathf.Lerp(request.From.y, y, f);

            i++;
                        
                        
            texture.SetPixels(lerpX, lerpY, request.Size, request.Size,request.Colors);
            
        }
        
    }
}
