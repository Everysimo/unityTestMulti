using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Oculus.Interaction;
using Oculus.Interaction.HandGrab;
using Oculus.Interaction.Input;
using Unity.VisualScripting;
using UnityEngine;

public class WhiteboardMarker : MonoBehaviour
{
    [SerializeField] private Transform _tip;
    [SerializeField] protected int _penSize = 5;

    private Renderer _renderer;

    private Grabbable _ovrGrabbale;

    protected Color[] _colors;
    private float _tipHeight;

    private RaycastHit _touch;

    protected Whiteboard _whiteboard;

    private Vector2 _touchPos;
    private Vector2 _lastTouchPos;

    private Quaternion _lastTouchRot;

    private Color _whiteboardColor;

    private bool _touchedLastFrame;

    public bool isEreaser = false;

    public AudioClip writingSound;

    public AudioSource _AudioSource;

    [SerializeField] HandGrabInteractor grabInteractorRight;
    [SerializeField] HandGrabInteractor grabInteractorLeft;

    void Start()
    {
        _renderer = _tip.GetComponent<Renderer>();

        _AudioSource = GetComponent<AudioSource>();
        _AudioSource.loop = true;

        _ovrGrabbale = transform.GetComponent<Grabbable>();

        _colors = Enumerable.Repeat(_renderer.material.color, _penSize * _penSize).ToArray();
        _tipHeight = _tip.localScale.y;
    }

    OVRInput.Controller ControllerThatIsGrabbing()
    {
        if (grabInteractorLeft.IsGrabbing && grabInteractorLeft.Interactable.transform.parent.Equals(transform))
        {
            return OVRInput.Controller.LTouch;
        }
        else if (grabInteractorRight.IsGrabbing && grabInteractorRight.Interactable.transform.parent.Equals(transform))
        {
            return OVRInput.Controller.RTouch;
        }

        return OVRInput.Controller.None;
    }

    void FixedUpdate()
    {
        //se stiamo toccando la lavagno cambiamo il render della lavagna
        Draw();
    }

    // ReSharper disable Unity.PerformanceAnalysis
    private void Draw()
    {
        if (!Physics.Raycast(_tip.position, transform.up, out _touch, _tipHeight))
        {
            _whiteboard = null;
            _touchedLastFrame = false;
            return;
        }
        //se tocchiamo qualcosa dalla poszione della punta e si va versp l'alto allora facciamo questo

        if (!_touch.transform.CompareTag("Whiteboard"))
        {
            return;
        }

        //se non è in chache la creiamo altimenti passiamo avanti
        if (_whiteboard == null)
        {
            SetWhiteboard(_touch.transform.GetComponent<Whiteboard>());
        }


        _touchPos = new Vector2(_touch.textureCoord.x, _touch.textureCoord.y);

        var x = (int)(_touchPos.x * _whiteboard.textureSize.x - (_penSize / 2));
        var y = (int)(_touchPos.y * _whiteboard.textureSize.y - (_penSize / 2));

        //se siamo fuori dalla lavagna smettiamo di disegnare
        if (y < 0 || y > _whiteboard.textureSize.y - 5 || x < 0 || x > _whiteboard.textureSize.x - 5)
        {
            return;
        }

        if (_touchedLastFrame)
        {
            var touchCord = new Vector2(x, y);

            DrawRequest drawRequest = new DrawRequest(_penSize, _colors, _lastTouchPos, touchCord);
            SendRequest(drawRequest);


            VibrationManager.singleton.TriggerVibration(20, 2, 1, ControllerThatIsGrabbing());
            _AudioSource.Play();
        }

        _AudioSource.Stop();
        _lastTouchPos = new Vector2(x, y);
        _touchedLastFrame = true;
    }


    private void SetWhiteboard(Whiteboard whiteboard)
    {
        _whiteboard = whiteboard;


        if (isEreaser)
        {
            _whiteboardColor = whiteboard.transform.GetComponent<Renderer>().material.color;

            if (_whiteboardColor == null)
            {
                _whiteboardColor = Color.white;
            }

            _colors = Enumerable.Repeat(_whiteboardColor, _penSize * _penSize).ToArray();
        }
    }

    protected virtual void SendRequest(DrawRequest drawRequest)
    {
        if(_whiteboard != null)
            _whiteboard.AddDrawRequest(drawRequest);
    }
    
}