using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NetworkWhiteboard : MonoBehaviourPunCallbacks
{

    private Whiteboard _whiteboard;
    private void Awake()
    {
        _whiteboard = GetComponent<Whiteboard>();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            Color[] colors = _whiteboard.texture.GetPixels();
            float[] alphas = new float[colors.Length], reds = new float[colors.Length] , blues = new float[colors.Length], greens = new float[colors.Length];
            for (int i = 0; i < colors.Length; i++)
            {
                alphas[i] = colors[i].a;
                reds[i] = colors[i].r;
                greens[i] = colors[i].g;
                blues[i] = colors[i].b;
            }
            photonView.RPC("SetWhiteboardTexture",newPlayer,colors.Length,alphas as object,reds as object,blues as object,greens as object);
        }
        
    }

    [PunRPC]
    public void SetWhiteboardTexture(int count,float[] alphas,float[] reds,float[] blues,float[] greens)
    {
        Color[] colors = new Color[count];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = new Color(reds[i], greens[i], blues[i], alphas[i]);
        }
        _whiteboard.texture.SetPixels(colors);
        
        
    }
    
}
