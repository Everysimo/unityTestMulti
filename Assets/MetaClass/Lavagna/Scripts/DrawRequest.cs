using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Object = System.Object;

[Serializable]
public class DrawRequest
{
    public int Size { get; set; }

    public Color[] Colors { get; set; }

    public Vector2 From { get; set; }
    
    public Vector2 To { get; set; }

    public DrawRequest(int size, Color[] colors, Vector2 from, Vector2 to)
    {
        Size = size;
        Colors = colors;
        From = from;
        To = to;
    } 
    
    
    
    
    
    
    
    

    public static byte[] ObjectToByteArray(object obj)
    {
        BinaryFormatter bf = new BinaryFormatter();
        using (var ms = new MemoryStream())
        {
            bf.Serialize(ms, obj);
            return ms.ToArray();
        }
    }
		
    // Convert a byte array to an Object
    public static object ByteArrayToObject(byte[] arrBytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            memStream.Write(arrBytes, 0, arrBytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            var obj = binForm.Deserialize(memStream);
            return obj;
        }
    }
    
    
    
    
}